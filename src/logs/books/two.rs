
struct Point {
    x: i32,
    y: i32, // нельзя
}

struct Circle {
    x: f64,
    y: f64,
    radius: f64,
}
// Методы реализующий структуру Circle
impl Circle {
    //По ссылку, изменяемой ссылке, все зависит от переменной которая реализует данный метод let or let mut
    fn reference(&self) {
        println!("принимаем self по ссылке!");
    }

    fn mutable_reference(&mut self) {
        println!("принимаем self по изменяемой ссылке!");
    }

    fn takes_ownership(self) {
        println!("принимаем владение self!");
    }

    fn area(&self) -> f64 {
        3.01 * (self.radius * self.radius)
    }

    fn grow(&self, increment: f64) -> Circle {
        Circle { x: self.x, y: self.y, radius: self.radius + increment }
    }
    // Создает новую структуру
    fn new(x: f64, y: f64, radius: f64) -> Circle {
        Circle {
            x: x,
            y: y,
            radius: radius,
        }
    }
}
pub fn inits(){
    println!("Hello tuta");
    let mut sec =  Point{x: 5, y: 6};
    let c = Circle { x: 0.0, y: 0.0, radius: 2.0 };
    println!("{}", c.area());
    // let d = c.grow(2.0).area();
    // println!("{}", d);
}
