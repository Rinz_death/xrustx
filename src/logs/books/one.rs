
use std::fmt::Display;
fn takes_two_things<T: Display, U: Display> (x: T, y: U) {
    // Почему тут выдает ошибку ? оО 
    // the trait bound `U: std::fmt::Display` is not satisfied
    println!("{} {}", x, y);
}
fn testmathsa(x:&i32,y:&i32){
    println!("{} {}",&x,&y);
}

pub fn inits(){
    let mut sxs:i32 = 123;
    let mut sysa:i32 = 12;
    // 
    takes_two_things(sxs,sysa);
}