trait HasArea {
    fn area(&self) -> f64;
}

struct Circle {
    x: f64,
    y: f64,
    radius: f64,
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        3.02 * (self.radius * self.radius)
    }
}

struct Square {
    x: f64,
    y: f64,
    side: f64,
}
//Реализуем типаж HasArea для структуры Square
impl HasArea for Square {
    fn area(&self) -> f64 {
        self.side * self.side
    }
}
// Тип обязательно структура реализующая типаж HasArea
fn print_area<T: HasArea>(shape: T) {
    println!("Площадь этой фигуры равна {}", shape.area());
}

pub fn mains() {
    let c = Circle {
        x: 0.0f64,
        y: 0.0f64,
        radius: 1.0f64,
    };

    let s = Square {
        x: 0.0f64,
        y: 0.0f64,
        side: 1.0f64,
    };

    print_area(c);
    print_area(s);
}