extern crate mio;
use self::mio::*;

struct WebSocketServer;

impl Handler for WebSocketServer {
    type Timeout = usize;
    type Message = ();
}

pub fn main() {
    let mut event_loop = EventLoop::new().unwrap();
    // Создадим новый экземпляр структуры Handler:
    let mut handler = WebSocketServer;
    // ... и предоставим циклу событий изменяемую ссылку на него:
    event_loop.run(&mut handler).unwrap();
}